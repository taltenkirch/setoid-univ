{-# OPTIONS --without-K --prop #-}

module SetoidHom.Id where

open import Agda.Primitive
open import SetoidHom.lib
open import SetoidHom.CwF
open import SetoidHom.Props
open import lib


module _ {i}{Γ : Con i}{j}(A : Ty Γ j)(u : Tm Γ A) where

  module _ (v : Tm Γ A) where
    Id : Tm Γ (P j)
    ∣ Id ∣ γ = EL A γ ⊢ ∣ u ∣ γ ~ ∣ v ∣ γ
    ~ Id γ~ = mk↑pl ((λ { (mk↑ps uv) → trans3 (EL A _) (sym (EL A _) (~ u γ~)) (~ (subst A γ~) uv) (~ v γ~)})
                      ,p (λ { (mk↑ps uv) → trans3 (EL A _) (sym (EL A _) (~ u (sym Γ γ~))) ((~ (subst A (sym Γ γ~)) uv)) (~ v (sym Γ γ~)) }))

  idp : Tm Γ (ElP (Id u))
  ∣ idp ∣ γ = mk↑ps (ref (EL A _) _)
  ~ idp γ~ = ttp'

  module _ {k}(Id' : Ty (Γ ▷ A) k)(v : Tm Γ A)(e : Tm Γ (ElP (Id v)))(idp' : Tm Γ (Id' [ _,_ id {A = A} u ]T)) where

    recId : Tm Γ (Id' [  _,_ id {A = A} v ]T)
    ∣ recId ∣ γ =  ∣ subst Id' (ref Γ γ ,p trans (EL A γ) (subst-ref A _) (un↑ps (∣ e ∣ γ))) ∣ (∣ idp' ∣ γ)
    ~ recId γ~ = trans3 (EL Id' _) (sym (EL Id' _) (subst-trans Id' _ _ _))
                                     (subst-trans Id' _ _ _)
                                     (~ (subst Id' _) (~ idp' γ~))

-- weak β rule
recIdβ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u : Tm Γ A){k}(Id' : Ty (Γ ▷ A) k)(idp' : Tm Γ (Id' [ _,_ id {A = A} u ]T))
         → Tm Γ (ElP (Id _ (recId A u Id' u (idp A u) idp') idp'))
∣ recIdβ A u Id' idp' ∣ γ = mk↑ps (subst-ref Id' _)
~ (recIdβ A u Id' idp') _ = ttp'

module  _ {i}{Γ : Con i}{j}(A : Ty Γ j)(u : Tm Γ A){j}{Δ : Con j}{γ : Tms Δ Γ} where
  module _ (v : Tm Γ A) where
    Id[] : Id A u v [ γ ]t ≡ Id (A [ γ ]T) (u [ γ ]t) (v [ γ ]t)
    Id[] = refl

  open import AbbrevsHom

  module _ {k}(Id' : Ty (Γ ▷ A) k)(v : Tm Γ A)(e : Tm Γ (ElP (Id A u v)))(idp' : Tm Γ (Id' [ _,_ id {A = A} u ]T)) where
    recId[] : recId A u Id' v e idp' [ γ ]t ≡ recId (A [ γ ]T) (u [ γ ]t) (Id' [ γ ^ A ]T) (v [ γ ]t) (e [ γ ]t) (idp' [ γ ]t)
    recId[] = refl
