{-# OPTIONS --without-K --prop #-}

module SetoidHom.Props where

open import Agda.Primitive
open import SetoidHom.lib
open import SetoidHom.CwF

-- a universe of propositions

P : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
∣ EL (P {i} {Γ} j) γ ∣ = Prop j
_⊢_~_ (EL (P {i} {Γ} j) γ) a b = ↑pl ((↑ps a → b) ×p (↑ps b → a))
ref (EL (P {i} {Γ} j) γ) a = mk↑pl ((λ x → un↑ps x) ,p (λ x → un↑ps x))
sym (EL (P {i} {Γ} j) γ) = λ { (mk↑pl (f ,p g)) → mk↑pl (g ,p f) }
trans (EL (P {i} {Γ} j) γ) = λ { (mk↑pl (f ,p g)) (mk↑pl (f' ,p g')) → mk↑pl ((λ x → f' (mk↑ps (f x))) ,p (λ y → g (mk↑ps (g' y)))) }
subst (P {i} {Γ} j) γ₀₁ = ID _
subst-ref (P {i} {Γ} j) a = mk↑pl ((λ x → un↑ps x) ,p (λ x → un↑ps x))
subst-trans (P {i} {Γ} j) γ₀₁ γ₁₂ a = mk↑pl ((λ x → un↑ps x) ,p (λ x → un↑ps x))

ElP : ∀{i}{Γ : Con i}{j} → Tm Γ (P j) → Ty Γ j
∣ EL (ElP a) γ ∣ = ↑ps (∣ a ∣ γ)
_⊢_~_ (EL (ElP a) γ) _ _ = ⊤p'
ref (EL (ElP a) γ) _ = ttp'
sym (EL (ElP a) γ) _ = ttp'
trans (EL (ElP a) γ) _ _ = ttp'
∣ subst (ElP a) γ₀₁ ∣ α = mk↑ps (proj₁p (un↑pl (~ a γ₀₁)) α)
~ (subst (ElP a) γ₀₁) _ = ttp'
subst-ref (ElP a) _ = ttp'
subst-trans (ElP a) _ _ _ = ttp'

-- propositional truncation

Trunc : ∀{i}{Γ : Con i}{j} → Ty Γ j → Tm Γ (P j)
∣ Trunc A ∣ γ = Tr (∣ A ∣T γ)
~ (Trunc {Γ = Γ} A) γ~ = mk↑pl ((λ { (mk↑ps (tr α)) → tr (∣ subst A γ~ ∣ α) }) ,p (λ { (mk↑ps (tr α)) → tr (∣ subst A (sym Γ γ~) ∣ α) }))

trunc : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ A → Tm Γ (ElP (Trunc A))
∣ trunc t ∣ γ = mk↑ps (tr (∣ t ∣ γ))
~ (trunc t) _ = ttp'

untrunc : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ ElP (Trunc A)) (P k)} →
  Tm (Γ ▷ A) (ElP b [ _,_ (π₁ {A = A} id){A = ElP (Trunc A)} (trunc (π₂ {A = A} id)) ]T) →
  (u : Tm Γ (ElP (Trunc A))) → Tm Γ (ElP b [ _,_ id {A = ElP (Trunc A)} u ]T)
∣ untrunc t u ∣ γ = mk↑ps (untr (λ α → un↑ps (∣ t ∣ (γ ,Σ α))) (un↑ps (∣ u ∣ γ)))
~ (untrunc t u) _ = ttp'

-- -- lifting universe levels

-- LiftP : ∀{i}{Γ : Con i}{j}{k} → Tm Γ (P j) → Tm Γ (P (j ⊔ k))
-- ∣ LiftP {j = j}{k = k} a ∣ γ = ↑pl {j}{k} (∣ a ∣ γ)
-- ~ (LiftP a) γ~ = mk↑pl (
--   (λ α → mk↑pl (proj₁p (un↑pl (~ a γ~)) (mk↑ps (un↑pl (un↑ps α))))) ,p
--   (λ α → mk↑pl (proj₂p (un↑pl (~ a γ~)) (mk↑ps (un↑pl (un↑ps α))))))

-- liftP : ∀{i}{Γ : Con i}{j}{k}{a : Tm Γ (P j)} → Tm Γ (ElP a) → Tm Γ (ElP (LiftP {k = k} a))
-- ∣ liftP t ∣ γ = mk↑ps (mk↑pl (un↑ps (∣ t ∣ γ)))
-- ~ (liftP t) γ~ = ttp'

-- empty type

EmptyP : ∀{i}{Γ : Con i} → Tm Γ (P lzero)
∣ EmptyP ∣ _ = ⊥p
~ EmptyP _ = mk↑pl (un↑ps ,p un↑ps)

exfalsoP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ (ElP EmptyP) → Tm Γ A
∣ exfalsoP t ∣ γ = ⊥pelim (un↑ps (∣ t ∣ γ))
~ (exfalsoP t) {γ} _ = ⊥pelimp (un↑ps (∣ t ∣ γ))


-- sigma type

ΣP : ∀{i j k}{Γ : Con i}(a : Tm Γ (P j))(b : Tm (Γ ▷ ElP a) (P k)) → Tm Γ (P (j ⊔ k))
∣ ΣP {Γ} a b ∣ γ = Σp (∣ a ∣ γ) λ α → ∣ b ∣ (γ ,Σ mk↑ps α)
~ (ΣP {Γ} a b) {γ}{γ'} γ~ = mk↑pl ((λ { (mk↑ps (α ,p β)) → (proj₁p (un↑pl (~ a γ~)) (mk↑ps α)) ,p proj₁p (un↑pl (~ b (γ~ ,p ttp'))) (mk↑ps β) } )
                                    ,p λ { (mk↑ps (α ,p β)) → (proj₂p (un↑pl (~ a γ~)) (mk↑ps α)) ,p proj₂p (un↑pl (~ b (γ~ ,p ttp'))) (mk↑ps β) })

_,P_ : ∀{i j k}{Γ : Con i}{a : Tm Γ (P j)}{b : Tm (Γ ▷ ElP a) (P k)}(t : Tm Γ (ElP a))(u : Tm Γ (ElP b [ _,_ id {A = ElP a} t ]T)) → Tm Γ (ElP (ΣP a b))
∣ t ,P u ∣ γ = mk↑ps (un↑ps (∣ t ∣ γ) ,p un↑ps (∣ u ∣ γ))
~ (t ,P u) γ₀₁ = ttp'

-- proj₁P : {Γ : Con}{a : Tm Γ P}{b : Tm (Γ , ElP a) P} → Tm Γ (ElP (ΣP a b)) → Tm Γ (ElP a)
-- proj₁P w = record { ∣_∣ = λ γ → liftp (proj₁p (unliftp (∣ w ∣ γ))) }

-- proj₂P : {Γ : Con}{a : Tm Γ P}{b : Tm (Γ , ElP a) P}(w : Tm Γ (ElP (ΣP a b))) →
--   Tm Γ (ElP b [ < proj₁P {a = a}{b} w > ]T)
-- proj₂P w = record { ∣_∣ = λ γ → liftp (proj₂p (unliftp (∣ w ∣ γ))) }

-- ΣPβ₁ : {Γ : Con}{a : Tm Γ P}{b : Tm (Γ , ElP a) P}{t : Tm Γ (ElP a)}{u : Tm Γ (ElP b [ < t > ]T)} →
--   proj₁P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ t
-- ΣPβ₁ = refl

-- ΣPβ₂ : {Γ : Con}{a : Tm Γ P}{b : Tm (Γ , ElP a) P}{t : Tm Γ (ElP a)}{u : Tm Γ (ElP b [ < t > ]T)}
--   → proj₂P {a = a}{b} (_,P_ {a = a}{b} t u) ≡ u
-- ΣPβ₂ = refl

-- ΣP[] : {Γ : Con}{a : Tm Γ P}{b : Tm (Γ , ElP a) P}{Θ : Con}{σ : Tms Θ Γ} →
--   ΣP a b [ σ ]t ≡ ΣP (a [ σ ]t) (b [ σ ^ ElP a ]t)
-- ΣP[] = refl

-- ,P[] : {Γ : Con}{a : Tm Γ P}{b : Tm (Γ , ElP a) P}{t : Tm Γ (ElP a)}{u : Tm Γ (ElP b [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
--   (_,P_ {a = a}{b} t u) [ σ ]t ≡ _,P_ {a = a [ σ ]t}{b = b [ σ ^ ElP a ]t} (t [ σ ]t) (u [ σ ]t)
-- ,P[] = refl


-- pi type

πsp : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm (Γ ▷ A) (P k)) → Tm Γ (P (j ⊔ k))
∣ πsp A b ∣ γ = (α : ∣ A ∣T γ) → ∣ b ∣ (γ ,Σ α)
~ (πsp {Γ = Γ} A b) γ~ = mk↑pl ((λ f α' → proj₁p (un↑pl (~ b (γ~ ,p trans (EL A _) (sym (EL A _) (subst-trans A _ _ _)) (subst-ref A _)))) (mk↑ps (un↑ps f (∣ subst A (sym Γ γ~) ∣ α'))))
                                ,p λ f' α → proj₂p (un↑pl (~ b (γ~ ,p ref (EL A _) _))) (mk↑ps (un↑ps f' (∣ subst A γ~ ∣ α))))

πpp : ∀{i}{Γ : Con i}{j}(a : Tm Γ (P j)){k}(b : Tm (Γ ▷ ElP a) (P k)) → Tm Γ (P (j ⊔ k))
∣ πpp a b ∣ γ = (α : ∣ a ∣ γ) → ∣ b ∣ (γ ,Σ (mk↑ps α))
~ (πpp {Γ = Γ} a b) γ~ = mk↑pl ((λ f α' → proj₁p (un↑pl (~ b (γ~ ,p ttp'))) (mk↑ps (un↑ps f (proj₂p (un↑pl (~ a γ~)) (mk↑ps α')))) )
                                ,p λ f' α → proj₂p (un↑pl (~ b (γ~ ,p ttp'))) (mk↑ps (un↑ps f' ((proj₁p (un↑pl (~ a γ~)) (mk↑ps α))))))

-- lamP : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) P} → Tm (Γ , A) (ElP b) → Tm Γ (ElP (ΠP A b))
-- lamP {Γ}{A}{b} t = record { ∣_∣ = λ γ → liftp λ α → unliftp (∣ t ∣ (γ ,Σ α)) }

-- appP : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) P} → Tm Γ (ElP (ΠP A b)) → Tm (Γ , A) (ElP b)
-- appP {Γ}{A}{b} t = record { ∣_∣ = λ { (γ ,Σ α) → liftp (unliftp (∣ t ∣ γ) α) } }

-- ΠPβ : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) P}{t : Tm (Γ , A) (ElP b)} → appP {A = A}{b} (lamP {A = A}{b} t) ≡ t
-- ΠPβ = refl

-- ΠPη : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) P}{t : Tm Γ (ElP (ΠP A b))} → lamP {A = A}{b} (appP {A = A}{b} t) ≡ t
-- ΠPη = refl

-- ΠP[] : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) P}{Θ : Con}{σ : Tms Θ Γ} → ΠP A b [ σ ]t ≡ ΠP (A [ σ ]T) (b [ σ ^ A ]t)
-- ΠP[] = refl

-- lamP[] : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) P}{t : Tm (Γ , A) (ElP b)}{Θ : Con}{σ : Tms Θ Γ} →
--   lamP {A = A}{b} t [ σ ]t ≡ lamP {A = A [ σ ]T}{b [ σ ^ A ]t}(t [ σ ^ A ]t)
-- lamP[] = refl
