{-# OPTIONS --without-K --prop --postfix-projections #-}

module SetoidHom.UP2 where

open import Agda.Primitive
open import lib
open import SetoidHom.lib
open import SetoidHom.CwF
open import SetoidHom.Props
open import SetoidHom.Id
open import AbbrevsHom

private variable
  i j : Level
  Γ Δ : Con i
  A : Ty Γ i
  ∣A∣ ∣A∣₀ ∣A∣₁ : Set i
  x₀ x₁ y₀ y₁ : ∣A∣

data ∣UP∣ (∣A∣ : Set i) : Set i where
  ∣pair∣ : ∣A∣ → ∣A∣ → ∣UP∣ ∣A∣

private variable p₀ p₁ p₂ : ∣UP∣ ∣A∣

data UP~ {∣A∣ : Set i} (A~ : ∣A∣ → ∣A∣ → Prop i) : ∣UP∣ ∣A∣ → ∣UP∣ ∣A∣ → Prop i
  where
  refUP : ∀ p → UP~ A~ p p
  symUP : UP~ A~ p₀ p₁ → UP~ A~ p₁ p₀
  transUP : UP~ A~ p₀ p₁ → UP~ A~ p₁ p₂ → UP~ A~ p₀ p₂

  pair~ : A~ x₀ x₁ → A~ y₀ y₁ → UP~ A~ (∣pair∣ x₀ y₀) (∣pair∣ x₁ y₁)
  ∣eq∣ : ∀ x y → UP~ A~ (∣pair∣ x y) (∣pair∣ y x)

module ∣Rec∣ (∣UP∣ᴾ : Set i) (∣pair∣ᴾ : ∣A∣ → ∣A∣ → ∣UP∣ᴾ) where
  rec : ∣UP∣ ∣A∣ → ∣UP∣ᴾ
  rec (∣pair∣ x y) = ∣pair∣ᴾ x y

map∣UP∣ : (∣A∣₀ → ∣A∣₁) → ∣UP∣ ∣A∣₀ → ∣UP∣ ∣A∣₁
map∣UP∣ ∣A∣₀₁ (∣pair∣ x₀ y₀) = ∣pair∣ (∣A∣₀₁ x₀) (∣A∣₀₁ y₀)

∣substUP∣ :
  ∀ (A : Ty Γ i) {γ₀ γ₁} →
  Γ ⊢ γ₀ ~ γ₁ → ∣UP∣ ∣ A .EL γ₀ ∣ → ∣UP∣ ∣ A .EL γ₁ ∣
∣substUP∣ A γ₀₁ = map∣UP∣ ∣ A .subst γ₀₁ ∣

substUP~ :
  ∀ (A : Ty Γ i) {γ₀ γ₁} (γ₀₁ : Γ ⊢ γ₀ ~ γ₁) {p₀ p₁ : ∣UP∣ ∣ A .EL γ₀ ∣} →
  UP~ (A .EL γ₀ ⊢_~_) p₀ p₁ →
  UP~ (A .EL γ₁ ⊢_~_) (∣substUP∣ A γ₀₁ p₀) (∣substUP∣ A γ₀₁ p₁)
substUP~ A γ₀₁ (refUP p) = refUP (∣substUP∣ A γ₀₁ p)
substUP~ A γ₀₁ (symUP p₀₁) = symUP (substUP~ A γ₀₁ p₀₁)
substUP~ A γ₀₁ (transUP p₀₁ p₁₂) =
  transUP (substUP~ A γ₀₁ p₀₁) (substUP~ A γ₀₁ p₁₂)
substUP~ A γ₀₁ (pair~ x₀₁ y₀₁) =
  pair~ (A .subst γ₀₁ .~ x₀₁) (A .subst γ₀₁ .~ y₀₁)
substUP~ A γ₀₁ (∣eq∣ x y) = ∣eq∣ (∣ A .subst γ₀₁ ∣ x) (∣ A .subst γ₀₁ ∣ y)

UP : Ty Γ i → Ty Γ i
∣ UP A .EL γ ∣ = ∣UP∣ ∣ A .EL γ ∣
UP A .EL γ ._⊢_~_ = UP~ (A .EL γ ⊢_~_)
UP A .EL γ .ref = refUP
UP A .EL γ .sym = symUP
UP A .EL γ .trans = transUP
∣ UP A .subst γ₀₁ ∣ = ∣substUP∣ A γ₀₁
UP A .subst γ₀₁ .~ = substUP~ A γ₀₁
UP A .subst-ref (∣pair∣ x y) = pair~ (A .subst-ref x) (A .subst-ref y)
UP A .subst-trans γ₀₁ γ₁₂ (∣pair∣ x y) =
  pair~ (A .subst-trans γ₀₁ γ₁₂ x) (A .subst-trans γ₀₁ γ₁₂ y)

pair : Tm Γ A → Tm Γ A → Tm Γ (UP A)
∣ pair x y ∣ γ = ∣pair∣ (∣ x ∣ γ) (∣ y ∣ γ)
pair x y .~ γ₀₁ = pair~ (x .~ γ₀₁) (y .~ γ₀₁)

eq : (x y : Tm Γ A) → Tm Γ (ElP (Id (UP A) (pair x y) (pair y x)))
∣ eq x y ∣ γ .un↑ps = ∣eq∣ (∣ x ∣ γ) (∣ y ∣ γ)
eq x y .~ γ₀₁ .un↑pl = tr tt

module Rec
  (A : Ty Γ i)
  (UPᴾ : Ty Γ j)
  (pairᴾ : Tm (Γ ▷ A ▷ A [ wk' A ]T) (UPᴾ [ wk' A ∘ wk' (A [ wk' A ]T) ]T))
  (eqᴾ :
    Tm
      (Γ ▷ A ▷ A [ wk' A ]T)
      (ElP
        (Id
          (UPᴾ [ wk' A ∘ wk' (A [ wk' A ]T) ]T)
          pairᴾ
          (pairᴾ [ wk' A ^ A ,⟨ A [ wk' A ]T ⟩ vs' (A [ wk' A ]T) (vz' A) ]t))))
  where
  module ∣R∣ {γ} = ∣Rec∣ ∣ UPᴾ .EL γ ∣ (λ x y → ∣ pairᴾ ∣ (γ ,Σ x ,Σ y))

  rec~ :
    ∀ {γ} {p₀ : ∣ UP A .EL γ ∣} {p₁ : ∣ UP A .EL γ ∣} →
    UP~ (A .EL γ ⊢_~_) p₀ p₁ →
    UPᴾ .EL γ ⊢ ∣R∣.rec p₀ ~ ∣R∣.rec p₁
  rec~ {γ} (refUP p) = UPᴾ .EL γ .ref (∣R∣.rec p)
  rec~ {γ} (symUP p₀₁) = UPᴾ .EL γ .sym (rec~ p₀₁)
  rec~ {γ} (transUP p₀₁ p₁₂) = UPᴾ .EL γ .trans (rec~ p₀₁) (rec~ p₁₂)
  rec~ {γ} (pair~ x₀₁ y₀₁) =
    UPᴾ .EL γ .trans
      (UPᴾ .EL γ .sym (UPᴾ .subst-ref _))
      (pairᴾ .~
        (ref Γ γ
          ,p A .EL γ .trans (A .subst-ref _) x₀₁
          ,p A .EL γ .trans (A .subst-ref _) y₀₁))
  rec~ {γ} (∣eq∣ x y) = ∣ eqᴾ ∣ (γ ,Σ x ,Σ y) .un↑ps

  ∣rec∣~ :
    ∀ {γ₀ γ₁} {γ₀₁ : Γ ⊢ γ₀ ~ γ₁} p →
    UPᴾ .EL γ₁
      ⊢ ∣ UPᴾ .subst γ₀₁ ∣ (∣R∣.rec p)
      ~ (∣R∣.rec (map∣UP∣ ∣ A .subst γ₀₁ ∣ p))
  ∣rec∣~ {γ₀} {γ₀₁ = γ₀₁} (∣pair∣ x y) =
    pairᴾ .~
      (γ₀₁
        ,p A .subst γ₀₁ .~ (A .EL γ₀ .ref x)
        ,p A .subst γ₀₁ .~ (A .EL γ₀ .ref y))

  rec : Tm Γ (UP A) → Tm Γ UPᴾ
  ∣ rec p ∣ γ = ∣R∣.rec (∣ p ∣ γ)
  rec p .~ {γ₀} {γ₁} γ₀₁ = UPᴾ .EL γ₁ .trans (∣rec∣~ (∣ p ∣ γ₀)) (rec~ (p .~ γ₀₁))

module _ {σ : Tms Γ Δ} {A : Ty Δ i} where
  UP[] : UP A [ σ ]T ≡ UP (A [ σ ]T)
  UP[] = refl

  pair[] : {x y : Tm Δ A} → pair x y [ σ ]t ≡ pair (x [ σ ]t) (y [ σ ]t)
  pair[] = refl

  module SubstRec
    (UPᴾ : Ty Δ j)
    (pairᴾ : Tm (Δ ▷ A ▷ A [ wk' A ]T) (UPᴾ [ wk' A ∘ wk' (A [ wk' A ]T) ]T))
    (eqᴾ :
      Tm
        (Δ ▷ A ▷ A [ wk' A ]T)
        (ElP
          (Id
            (UPᴾ [ wk' A ∘ wk' (A [ wk' A ]T) ]T)
            pairᴾ
            (pairᴾ
              [ wk' A ^ A ,⟨ A [ wk' A ]T ⟩ vs' (A [ wk' A ]T) (vz' A) ]t))))
    where
    module RΓ =
      Rec
        (A [ σ ]T)
        (UPᴾ [ σ ]T)
        (pairᴾ [ σ ^ A ^ A [ wk' A ]T ]t)
        (eqᴾ [ σ ^ A ^ A [ wk' A ]T ]t)
    module RΔ = Rec A UPᴾ pairᴾ eqᴾ

    rec[] : ∀ {p} → RΔ.rec p [ σ ]t ≡ RΓ.rec (p [ σ ]t)
    rec[] = refl
