{-# OPTIONS --without-K --prop #-}

module SetoidHom.Bool where

open import lib
open import Agda.Primitive

open import SetoidHom.lib
open import SetoidHom.CwF


Bool : ∀{i}{Γ : Con i} → Ty Γ lzero
Bool = record {
  EL = λ _ → record {
    ∣_∣ = 𝟚 ;
    _⊢_~_ = λ t₀ t₁ → if t₀ then (if t₁ then ⊤p else ⊥p) else (if t₁ then ⊥p else ⊤p) ;
    ref = λ t → pif_then_else_ {C = λ t → if t then (if t then ⊤p else ⊥p) else (if t then ⊥p else ⊤p)} t ttp ttp ;
    sym = λ {t₀}{t₁} t₀₁ →
      pif_then_else_
        {C = λ t₀ → if t₀ then if t₁ then ⊤p else ⊥p else (if t₁ then ⊥p else ⊤p) → if t₁ then if t₀ then ⊤p else ⊥p else (if t₀ then ⊥p else ⊤p)}
        t₀
        (λ x → x)
        (λ x → x)
        t₀₁ ;
    trans = λ {t₀}{t₁}{t₂} t₀₁ t₁₂ →
      pif_then_else_
        {C = λ t₀ → if t₀ then if t₁ then ⊤p else ⊥p else (if t₁ then ⊥p else ⊤p) → if t₁ then if t₂ then ⊤p else ⊥p else (if t₂ then ⊥p else ⊤p) → if t₀ then if t₂ then ⊤p else ⊥p else (if t₂ then ⊥p else ⊤p)}
        t₀
        (λ x y → 
          pif_then_else_
            {C = λ t₁ → if t₁ then ⊤p else ⊥p → if t₁ then if t₂ then ⊤p else ⊥p else (if t₂ then ⊥p else ⊤p) → if t₂ then ⊤p else ⊥p}
            t₁ (λ _ x → x) (λ ()) x y)
        (λ x y →
          pif_then_else_
            {C = λ t₁ → if t₁ then ⊥p else ⊤p → if t₁ then if t₂ then ⊤p else ⊥p else (if t₂ then ⊥p else ⊤p) → if t₂ then ⊥p else ⊤p}
            t₁ (λ ()) (λ _ x → x) x y)
        t₀₁
        t₁₂ } ;
  subst = λ γ~ → id ;
  subst-ref = λ t → pif_then_else_ {C = λ t → if t then (if t then ⊤p else ⊥p) else (if t then ⊥p else ⊤p)} t ttp ttp ;
  subst-trans = λ _ _ t → pif_then_else_ {C = λ t → if t then (if t then ⊤p else ⊥p) else (if t then ⊥p else ⊤p)} t ttp ttp }

true    : ∀{i}{Γ : Con i} → Tm Γ Bool
true = record { ∣_∣ = λ _ → tt ; ~ = λ _ → ttp }
false   : ∀{i}{Γ : Con i} → Tm Γ Bool
false = record { ∣_∣ = λ _ → ff ; ~ = λ _ → ttp }

ite :
  ∀{i}{Γ : Con i}{j}(C : Ty (Γ ▷ Bool) j)
      → Tm Γ (C [ (_,_ id {A = Bool} true) ]T)
      → Tm Γ (C [ (_,_ id {A = Bool} false) ]T)
      → (t : Tm Γ Bool)
      → Tm Γ (C [ (_,_ id {A = Bool} t) ]T)
ite {i}{Γ}{j} = λ C u v t → record {
  ∣_∣ = λ γ → if_then_else_ {C = λ b → ∣ EL C (γ ,Σ b) ∣} (∣ t ∣ γ) (∣ u ∣ γ) (∣ v ∣ γ) ;
  ~   = λ {γ}{γ'} γ~ → pif_then_else_
      {j}
      {C = λ b → (b~ : EL {i}{Γ} Bool γ' ⊢ b ~ ∣ t ∣ γ') → EL C (γ' ,Σ ∣ t ∣ γ') ⊢ ∣ subst C (γ~ ,p b~) ∣ (if_then_else_ {C = λ b → ∣ EL C (γ ,Σ b) ∣} b (∣ u ∣ γ) (∣ v ∣ γ)) ~ (if_then_else_ {C = λ b → ∣ EL C (γ' ,Σ b) ∣} (∣ t ∣ γ') (∣ u ∣ γ') (∣ v ∣ γ'))}
      (∣ t ∣ γ)
      (pif_then_else_ {C = λ b → (b~ : EL {i}{Γ} Bool γ' ⊢ tt ~ b) → EL C (γ' ,Σ b) ⊢ ∣ subst C (γ~ ,p b~) ∣ (∣ u ∣ γ) ~ (if_then_else_ {C = λ b → ∣ EL C (γ' ,Σ b) ∣} b (∣ u ∣ γ') (∣ v ∣ γ'))} (∣ t ∣ γ') (λ _ → ~ u γ~) (λ ()))
      (pif_then_else_ {C = λ b → (b~ : EL {i}{Γ} Bool γ' ⊢ ff ~ b) → EL C (γ' ,Σ b) ⊢ ∣ subst C (γ~ ,p b~) ∣ (∣ v ∣ γ) ~ (if_then_else_ {C = λ b → ∣ EL C (γ' ,Σ b) ∣} b (∣ u ∣ γ') (∣ v ∣ γ'))} (∣ t ∣ γ') (λ ()) (λ _ → ~ v γ~))
      (~ t γ~) }

Bool[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{γ : Tms Δ Γ} → Bool [ γ ]T ≡ Bool
Bool[] = refl

true[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{γ : Tms Δ Γ} → true [ γ ]t ≡ true
true[] = refl

false[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{γ : Tms Δ Γ} → false [ γ ]t ≡ false
false[] = refl

open import AbbrevsHom

ite[] : ∀{i}{Γ : Con i}{j}{C : Ty (Γ ▷ Bool) j}{u : Tm Γ (C [ (_,_ id {A = Bool} true) ]T)}{v : Tm Γ (C [ (_,_ id {A = Bool} false) ]T)}{t : Tm Γ Bool}{k}{Δ : Con k}{γ : Tms Δ Γ} →
  ite C u v t [ γ ]t ≡ ite (C [ γ ^ Bool ]T) (u [ γ ]t) (v [ γ ]t) (t [ γ ]t)
ite[] = refl
