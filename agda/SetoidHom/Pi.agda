{-# OPTIONS --without-K --prop #-}

module SetoidHom.Pi where

open import Agda.Primitive
open import SetoidHom.lib
open import SetoidHom.CwF

module _ {i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) where
  Π : Ty Γ (j ⊔ k)
  Π = record {
    EL = λ γ → record {
      ∣_∣ = Σsp ((α : ∣ EL A γ ∣) → ∣ EL B (γ ,Σ α) ∣) λ f →
                 {α α' : ∣ EL A γ ∣}(α~ : EL A γ ⊢ α ~ α') → EL B (γ ,Σ α') ⊢ ∣ subst B (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~) ∣ (f α) ~ f α' ;
      _⊢_~_ = λ { (f ,sp _)(f' ,sp _) → (α α' : ∣ EL A γ ∣)(α~ : EL A γ ⊢ α ~ α') → EL B (γ ,Σ α') ⊢ ∣ subst B (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~) ∣ (f α) ~ f' α' } ;
      ref = λ { (f ,sp reff) α α' α~ → reff α~ } ;
      sym = λ { {f ,sp reff}{f' ,sp reff'} f~ α α' α~ → trans (EL B (γ ,Σ α'))
        (~ (subst B (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~)) (sym (EL B (γ ,Σ α)) (f~ _ _ (sym (EL A γ) α~))))
        (trans (EL B (γ ,Σ α')) (sym (EL B (γ ,Σ α')) (subst-trans B (ref Γ γ ,p trans (EL A γ) (subst-ref A α') (sym (EL A γ) α~)) (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~) (f α')))
        (subst-ref B (f α'))) } ;
      trans = λ { {f ,sp reff}{f' ,sp reff'}{f'' ,sp reff''} f~ f~' α α'' α~ → trans (EL B (γ ,Σ α''))
        (~ (subst B (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~)) (trans (EL B (γ ,Σ α))
          (sym (EL B (γ ,Σ α)) (subst-ref B {γ ,Σ α} (f α)))
          (f~ _ _ (ref (EL A γ) α))))
        (f~' _ _ α~) } } ;
    subst = λ {γ}{γ'} γ~ → record {
      ∣_∣ = λ { (f ,sp reff) →
        (λ α' → ∣ subst B (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α')) (subst-ref A α')) ∣ (f (∣ subst A (sym Γ γ~) ∣ α'))) ,sp
        λ {α}{α'} α~ → trans (EL B (γ' ,Σ α'))
          (trans (EL B (γ' ,Σ α'))
            (sym (EL B (γ' ,Σ α'))
              (subst-trans B
                (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α)) (subst-ref A α))
                (ref Γ γ' ,p trans (EL A γ') (subst-ref A α) α~)
                (f (∣ subst A (sym Γ γ~) ∣ α))))
            (subst-trans B
              (ref Γ γ ,p trans (EL A γ) (subst-ref A (∣ subst A (sym Γ γ~) ∣ α)) (~ (subst A (sym Γ γ~)) α~))
              (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α')) (subst-ref A α'))
              (f (∣ subst A (sym Γ γ~) ∣ α))))
          (~
            (subst B (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α')) (subst-ref A α')))
            (reff (~ (subst A (sym Γ γ~)) α~))) } ;
      ~ = λ { {f ,sp reff}{f' ,sp reff'} f~ α α' α~ → trans (EL B (γ' ,Σ α')) (trans (EL B (γ' ,Σ α'))
        (sym (EL B (γ' ,Σ α')) (subst-trans B
          (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α)) (subst-ref A α))
          (ref Γ γ' ,p trans (EL A γ') (subst-ref A α) α~)
          (f (∣ subst A (sym Γ γ~) ∣ α))))
        (subst-trans B
          (ref Γ γ ,p trans (EL A γ) (subst-ref A (∣ subst A (sym Γ γ~) ∣ α)) (~ (subst A (sym Γ γ~)) α~))
          (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α')) (subst-ref A α'))
          (f (∣ subst A (sym Γ γ~) ∣ α))))
        (~ (subst B (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α')) (subst-ref A α'))) (f~ _ _ (~ (subst A (sym Γ γ~)) α~))) } } ;
    subst-ref = λ { {γ}(f ,sp reff) α α' α~ → trans (EL B (γ ,Σ α'))
      (sym (EL B (γ ,Σ α')) (subst-trans B (ref Γ γ ,p trans (EL A γ) (sym (EL A γ) (subst-trans A (sym Γ (ref Γ γ)) (ref Γ γ) α)) (subst-ref A α)) (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~) (f (∣ subst A (sym Γ (ref Γ γ)) ∣ α)))) (trans (EL B (γ ,Σ α')) (trans (EL B (γ ,Σ α'))
      (subst-trans B (ref Γ γ ,p trans (EL A γ) (subst-ref A (∣ subst A (ref Γ γ) ∣ α)) (subst-ref A α)) (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~) (f (∣ subst A (ref Γ γ) ∣ α)))
      (~ (subst B (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~)) (reff (subst-ref A α))))
      (reff α~)) } ;
    subst-trans = λ { {γ}{γ'}{γ''} γ~ γ~' (f ,sp reff) α α'' α~ → trans (EL B (γ'' ,Σ α''))
      (sym (EL B (γ'' ,Σ α'')) (subst-trans B (trans Γ γ~ γ~' ,p trans (EL A γ'') (sym (EL A γ'') (subst-trans A (sym Γ (trans Γ γ~ γ~')) (trans Γ γ~ γ~') α)) (subst-ref A α)) (ref Γ γ'' ,p trans (EL A γ'') (subst-ref A α) α~) (f (∣ subst A (sym Γ (trans Γ γ~ γ~')) ∣ α)))) (trans (EL B (γ'' ,Σ α'')) (trans (EL B (γ'' ,Σ α'')) (trans (EL B (γ'' ,Σ α'')) (trans (EL B (γ'' ,Σ α''))
      (subst-trans B (ref Γ γ ,p trans (EL A γ) (subst-ref A (∣ subst A (sym Γ (trans Γ γ~ γ~')) ∣ α)) (~ (subst A (sym Γ (trans Γ γ~ γ~'))) α~)) (trans Γ (ref Γ γ) (trans Γ γ~ γ~') ,p trans (EL A γ'') (subst-trans A (ref Γ γ) (trans Γ γ~ γ~') (∣ subst A (trans Γ (sym Γ γ~') (sym Γ γ~)) ∣ α'')) (trans (EL A γ'') (~ (subst A (trans Γ γ~ γ~')) (trans (EL A γ) (subst-ref A (∣ subst A (trans Γ (sym Γ γ~') (sym Γ γ~)) ∣ α'')) (subst-trans A (sym Γ γ~') (sym Γ γ~) α''))) (trans (EL A γ'') (subst-trans A γ~ γ~' (∣ subst A (sym Γ γ~) ∣ (∣ subst A (sym Γ γ~') ∣ α''))) (trans (EL A γ'') (~ (subst A γ~') (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A (sym Γ γ~') ∣ α''))) (subst-ref A (∣ subst A (sym Γ γ~') ∣ α'')))) (trans (EL A γ'') (sym (EL A γ'') (subst-trans A (sym Γ γ~') γ~' α'')) (subst-ref A α'')))))) (f (∣ subst A (sym Γ (trans Γ γ~ γ~')) ∣ α)))
      (~ (subst B (trans Γ (ref Γ γ) (trans Γ γ~ γ~') ,p trans (EL A γ'') (subst-trans A (ref Γ γ) (trans Γ γ~ γ~') (∣ subst A (trans Γ (sym Γ γ~') (sym Γ γ~)) ∣ α'')) (trans (EL A γ'') (~ (subst A (trans Γ γ~ γ~')) (trans (EL A γ) (subst-ref A (∣ subst A (trans Γ (sym Γ γ~') (sym Γ γ~)) ∣ α'')) (subst-trans A (sym Γ γ~') (sym Γ γ~) α''))) (trans (EL A γ'') (subst-trans A γ~ γ~' (∣ subst A (sym Γ γ~) ∣ (∣ subst A (sym Γ γ~') ∣ α''))) (trans (EL A γ'') (~ (subst A γ~') (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A (sym Γ γ~') ∣ α''))) (subst-ref A (∣ subst A (sym Γ γ~') ∣ α'')))) (trans (EL A γ'') (sym (EL A γ'') (subst-trans A (sym Γ γ~') γ~' α'')) (subst-ref A α''))))))) (reff (~ (subst A (sym Γ (trans Γ γ~ γ~'))) α~))))
      (subst-trans B (ref Γ γ ,p trans (EL A γ) (subst-ref A (∣ subst A (trans Γ (sym Γ γ~') (sym Γ γ~)) ∣ α'')) (subst-trans A (sym Γ γ~') (sym Γ γ~) α'')) (trans Γ γ~ γ~' ,p trans (EL A γ'') (subst-trans A γ~ γ~' (∣ subst A (sym Γ γ~) ∣ (∣ subst A (sym Γ γ~') ∣ α''))) (trans (EL A γ'') (~ (subst A γ~') (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A (sym Γ γ~') ∣ α''))) (subst-ref A (∣ subst A (sym Γ γ~') ∣ α'')))) (trans (EL A γ'') (sym (EL A γ'') (subst-trans A (sym Γ γ~') γ~' α'')) (subst-ref A α'')))) (f (∣ subst A (trans Γ (sym Γ γ~') (sym Γ γ~)) ∣ α''))))
      (~ (subst B (trans Γ γ~ γ~' ,p trans (EL A γ'') (subst-trans A γ~ γ~' (∣ subst A (sym Γ γ~) ∣ (∣ subst A (sym Γ γ~') ∣ α''))) (trans (EL A γ'') (~ (subst A γ~') (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A (sym Γ γ~') ∣ α''))) (subst-ref A (∣ subst A (sym Γ γ~') ∣ α'')))) (trans (EL A γ'') (sym (EL A γ'') (subst-trans A (sym Γ γ~') γ~' α'')) (subst-ref A α''))))) (reff (subst-trans A (sym Γ γ~') (sym Γ γ~) α''))))
      (subst-trans B (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A (sym Γ γ~') ∣ α''))) (subst-ref A (∣ subst A (sym Γ γ~') ∣ α''))) (γ~' ,p trans (EL A γ'') (sym (EL A γ'') (subst-trans A (sym Γ γ~') γ~' α'')) (subst-ref A α'')) (f (∣ subst A (sym Γ γ~) ∣ (∣ subst A (sym Γ γ~') ∣ α''))))) } }

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
lam {Γ = Γ}{A = A}{B = B} t = record {
  ∣_∣ = λ γ → (λ α → ∣ t ∣ (γ ,Σ α)) ,sp λ {α}{α'} α~ → ~ t (ref Γ γ ,p trans (EL A γ) (subst-ref A α) α~) ;
  ~ = λ {γ}{γ'} γ~ α α' α~ → trans (EL B (γ' ,Σ α'))
    (sym (EL B (γ' ,Σ α')) (subst-trans B (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α)) (subst-ref A α)) (ref Γ γ' ,p trans (EL A γ') (subst-ref A α) α~) (∣ t ∣ (γ ,Σ ∣ subst A (sym Γ γ~) ∣ α))))
    (~ t (γ~ ,p trans (EL A γ') (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ α)) (subst-ref A α)) α~)) }

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
app {Γ = Γ}{A = A}{B = B} t = record {
  ∣_∣ = λ { (γ ,Σ α) → proj₁sp (∣ t ∣ γ) α } ;
  ~ = λ { {γ ,Σ α}{γ' ,Σ α'}(γ~ ,p α~) → trans (EL B (γ' ,Σ α')) (trans (EL B (γ' ,Σ α'))
    (trans (EL B (γ' ,Σ α'))
      (subst-trans B (ref Γ γ ,p trans (EL A γ) (subst-ref A α) (trans (EL A γ) (sym (EL A γ) (subst-ref A α)) (subst-trans A γ~ (sym Γ γ~) α))) (trans Γ γ~ (ref Γ γ') ,p trans (EL A γ') (subst-trans A γ~ (ref Γ γ') (∣ subst A (sym Γ γ~) ∣ (∣ subst A γ~ ∣ α))) (trans (EL A γ') (~ (subst A (ref Γ γ')) (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A γ~ ∣ α))) (subst-ref A (∣ subst A γ~ ∣ α)))) (trans (EL A γ') (subst-ref A (∣ subst A γ~ ∣ α)) α~))) (proj₁sp (∣ t ∣ γ) α))
      (~ (subst B (trans Γ γ~ (ref Γ γ') ,p trans (EL A γ') (subst-trans A γ~ (ref Γ γ') (∣ subst A (sym Γ γ~) ∣ (∣ subst A γ~ ∣ α))) (trans (EL A γ') (~ (subst A (ref Γ γ')) (trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A γ~ ∣ α))) (subst-ref A (∣ subst A γ~ ∣ α)))) (trans (EL A γ') (subst-ref A (∣ subst A γ~ ∣ α)) α~)))) (proj₂sp (∣ t ∣ γ) (trans (EL A γ) (sym (EL A γ) (subst-ref A α)) (subst-trans A γ~ (sym Γ γ~) α)))))
    (subst-trans B (γ~ ,p trans (EL A γ') (sym (EL A γ') (subst-trans A (sym Γ γ~) γ~ (∣ subst A γ~ ∣ α))) (subst-ref A (∣ subst A γ~ ∣ α))) (ref Γ γ' ,p trans (EL A γ') (subst-ref A (∣ subst A γ~ ∣ α)) α~) (proj₁sp (∣ t ∣ γ) (∣ subst A (sym Γ γ~) ∣ (∣ subst A γ~ ∣ α)))))
    (~ t γ~ _ _ α~) } }
