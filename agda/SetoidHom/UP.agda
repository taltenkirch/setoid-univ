{-# OPTIONS --without-K --prop #-}

module SetoidHom.UP where

open import lib
open import Agda.Primitive

open import SetoidHom.lib
open import SetoidHom.CwF
open import AbbrevsHom


module _ {i}(∣A∣ : Set i) where
  data ∣UP∣ : Set i where
    ∣pair∣ : ∣A∣ → ∣A∣ → ∣UP∣

  module _ (A~ : ∣A∣ → ∣A∣ → Prop i) where
    data UP~  : ∣UP∣ → ∣UP∣ → Prop i where
      pair~   : ∀{a₀₀ a₀₁} → A~ a₀₀ a₀₁ → ∀{a₁₀ a₁₁} → A~ a₁₀ a₁₁ → UP~ (∣pair∣ a₀₀ a₁₀) (∣pair∣ a₀₁ a₁₁)
      refUP   : ∀ u → UP~ u u
      symUP   : ∀{u₀ u₁} → UP~ u₀ u₁ → UP~ u₁ u₀
      transUP : ∀{u₀ u₁ u₂} → UP~ u₀ u₁ → UP~ u₁ u₂ → UP~ u₀ u₂
      eq      : ∀{a}{a'} → UP~ (∣pair∣ a a') (∣pair∣ a' a)

ite∣UP∣ : ∀{i}{∣A∣ : Set i}{j}{∣C∣ : Set j}(pairC : ∣A∣ → ∣A∣ → ∣C∣) → ∣UP∣ ∣A∣ → ∣C∣
ite∣UP∣ pairC (∣pair∣ a a') = pairC a a'

iteUP~ : ∀{i}{∣A∣ : Set i}{A~ : ∣A∣ → ∣A∣ → Prop i}
  {j}(C : Setoid j)
  {pairC : ∣A∣ → ∣A∣ → ∣ C ∣}(pairC~ : ∀{a₀ a₁} → A~ a₀ a₁ → ∀{a'₀ a'₁} → A~ a'₀ a'₁ → C ⊢ pairC a₀ a'₀ ~ pairC a₁ a'₁)
  (eqC : ∀{a a'} → C ⊢ pairC a a' ~ pairC a' a)
  {u₀ u₁} → UP~ ∣A∣ A~ u₀ u₁ → C ⊢ ite∣UP∣ pairC u₀ ~ ite∣UP∣ pairC u₁
iteUP~ C pairC~ eqC (pair~ a₀₁ a'₀₁) = pairC~ a₀₁ a'₀₁
iteUP~ C pairC~ eqC (refUP _) = ref C _
iteUP~ C pairC~ eqC (symUP u~) = sym C (iteUP~ C pairC~ eqC u~)
iteUP~ C pairC~ eqC (transUP u~ u~') = trans C (iteUP~ C pairC~ eqC u~) (iteUP~ C pairC~ eqC u~')
iteUP~ C pairC~ eqC eq = eqC

map : ∀{i}{∣A∣ : Set i}{j}{∣B∣ : Set j}(f : ∣A∣ → ∣B∣) → ∣UP∣ ∣A∣ → ∣UP∣ ∣B∣
map f = ite∣UP∣ (λ a a' → ∣pair∣ (f a) (f a'))

map~ : ∀{i}{∣A∣ : Set i}{A~ : ∣A∣ → ∣A∣ → Prop i}{j}{∣B∣ : Set j}{B~ : ∣B∣ → ∣B∣ → Prop j}
  {f : ∣A∣ → ∣B∣}(f~ : ∀{a₀ a₁} → A~ a₀ a₁ → B~ (f a₀) (f a₁)){u₀ u₁} →
  UP~ ∣A∣ A~ u₀ u₁ → UP~ ∣B∣ B~ (map f u₀) (map f u₁)
-- TODO: this should be definable using iteUP~
map~ f~ (pair~ a₀₌ a₁₌) = pair~ (f~ a₀₌) (f~ a₁₌)
map~ f~ (refUP _) = refUP _
map~ f~ (symUP u₀₁) = symUP (map~ f~ u₀₁)
map~ f~ (transUP u₀₁ u₁₂) = transUP (map~ f~ u₀₁) (map~ f~ u₁₂)
map~ f~ eq = eq

UP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Ty Γ j
EL (UP A) = λ γ₀ → record
  { ∣_∣    = ∣UP∣ ∣ EL A γ₀ ∣
  ; _⊢_~_  = UP~ ∣ EL A γ₀ ∣ (EL A γ₀ ⊢_~_)
  ; ref   = refUP
  ; sym   = symUP
  ; trans = transUP }
∣ subst (UP A) {γ₀} {γ₁} γ₀₁ ∣ = map (∣ subst A γ₀₁ ∣)
-- ∣ subst (UP A) {γ₀} {γ₁} γ₀₁ ∣ (∣pair∣ a₀₀ a₀₁) = ∣pair∣ (∣ subst A γ₀₁ ∣ a₀₀) (∣ subst A γ₀₁ ∣ a₀₁) -- with this we don't get definitional UP[], but it is still provable by funext
~ (subst (UP A) {γ₀} {γ₁} γ₀₁) = map~ (~ (subst A γ₀₁))
subst-ref (UP A) (∣pair∣ a₀₀ a₀₁) = pair~ (subst-ref A a₀₀) (subst-ref A a₀₁)
subst-trans (UP A) γ₀₁ γ₁₂ (∣pair∣ a₀₀ a₀₁) = pair~ (subst-trans A γ₀₁ γ₁₂ a₀₀) (subst-trans A γ₀₁ γ₁₂ a₀₁)

UP[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{Δ : Con k}{γ : Tms Δ Γ} → UP A [ γ ]T ≡ UP (A [ γ ]T)
UP[] = refl

pair : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ A → Tm Γ A → Tm Γ (UP A)
∣ pair a a' ∣ γ₀ = ∣pair∣ (∣ a ∣ γ₀) (∣ a' ∣ γ₀)
~ (pair a a') γ₀₁ = pair~ (~ a γ₀₁) (~ a' γ₀₁)

pair[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{a b : Tm Γ A}{k}{Δ : Con k}{γ : Tms Δ Γ} → pair a b [ γ ]t ≡ pair (a [ γ ]t) (b [ γ ]t)
pair[] = refl

-- a more heterogeneous version of iteUP~
iteUP~' : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(C : Ty Γ k)
  (pairC : Tm (Γ ▷ A ▷ A [ wk {A = A} ]T) (C [ wk {A = A} ∘ wk {A = A [ wk {A = A} ]T} ]T))
  (eqC : ∀{γ₀ a₀ a'₀} → {!!})
  {γ₀ γ₁}(γ₀₁ : Γ ⊢ γ₀ ~ γ₁)
  {u₀ : ∣ EL (UP A) γ₀ ∣}{u₁ : ∣ EL (UP A) γ₁ ∣}(u₀₁ : EL (UP A) γ₁ ⊢ ∣ subst (UP A) γ₀₁ ∣ u₀ ~ u₁) →
  EL C γ₁ ⊢ ∣ subst C γ₀₁ ∣ (ite∣UP∣ (λ a₀ a'₀ → ∣ pairC ∣ (γ₀ ,Σ a₀ ,Σ a'₀)) u₀) ~ ite∣UP∣ (λ a₀ a'₀ → ∣ pairC ∣ (γ₁ ,Σ a₀ ,Σ a'₀)) u₁
iteUP~' A C pairC eqC {γ₀}{γ₁} γ₀₁ {∣pair∣ a₀ a'₀} {∣pair∣ a₁ a'₁} (pair~ a₌ a'₌) = ~ pairC (γ₀₁ ,p a₌ ,p a'₌)
iteUP~' A C pairC eqC {γ₀}{γ₁} γ₀₁ {∣pair∣ a₀ a'₀} (refUP .(∣pair∣ (∣ subst A _ ∣ a₀) (∣ subst A _ ∣ a'₀))) = {!!}
iteUP~' {Γ = Γ} A C pairC eqC {γ₀}{γ₁} γ₀₁ {∣pair∣ a₀ a'₀} {∣pair∣ a₁ a'₁} (symUP u₀₁) = sym (EL C γ₁) {!iteUP~' A C pairC eqC (sym Γ γ₀₁) ?!} -- we have to move subst from one side to the other
iteUP~' A C pairC eqC {γ₀}{γ₁} γ₀₁ {∣pair∣ a₀ a'₀} {∣pair∣ a₁ a'₁} (transUP u₀₁ u₂) = {!!}
iteUP~' A C pairC eqC {γ₀}{γ₁} γ₀₁ {∣pair∣ a₀ a'₀} {∣pair∣ .(∣ subst A _ ∣ a'₀) .(∣ subst A _ ∣ a₀)} eq = {!!}

ite : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{C : Ty Γ k} → 
  Tm (Γ ▷ A ▷ A [ wk {A = A} ]T) (C [ wk {A = A} ∘ wk {A = A [ wk {A = A} ]T} ]T) →
  Tm Γ (UP A) → Tm Γ C
∣ ite pairC u ∣ γ₀ = ite∣UP∣ (λ a₀ a'₀ → ∣ pairC ∣ (γ₀ ,Σ a₀ ,Σ a'₀)) (∣ u ∣ γ₀)
~ (ite {A = A}{C = C} pairC u) {γ₀}{γ₁} γ₀₁ = {!!} -- TODO: it seems we need a more general version of iteUP~

ite[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{C : Ty Γ k}{t : Tm (Γ ▷ A ▷ A [ wk {A = A} ]T) (C [ wk {A = A} ∘ wk {A = A [ wk {A = A} ]T} ]T)}{u : Tm Γ (UP A)}{l}{Δ : Con l}{γ : Tms Δ Γ} →
  ite {A = A}{C = C} t u [ γ ]t ≡ ite {A = A [ γ ]T}{C = C [ γ ]T} (t [ γ ^ A ^ A [ wk {A = A} ]T ]t) (u [ γ ]t)
ite[] = refl

-- TODO: dependent eliminator
-- TODO: check more complicated QITs
