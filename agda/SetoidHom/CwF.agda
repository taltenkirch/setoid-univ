{-# OPTIONS --without-K --prop #-}

module SetoidHom.CwF where

open import Agda.Primitive
open import SetoidHom.lib

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t

Con = Setoid
Tms = SetoidMor
Ty  = SetoidFam
Tm  = SetoidSec

trans3 : ∀{i}(Γ : Con i){γ γ' γ'' γ''' : ∣ Γ ∣} → Γ ⊢ γ ~ γ' → Γ ⊢ γ' ~ γ'' → Γ ⊢ γ'' ~ γ''' → Γ ⊢ γ ~ γ'''
trans3 Γ γ~ γ~' γ~'' = trans Γ γ~ (trans Γ γ~' γ~'')

• : Con lzero
∣ • ∣ = ⊤
• ⊢ _ ~ _ = ⊤p
ref • _ = ttp
sym • _ = ttp
trans • _ _ = ttp

_▷_ : ∀{i}(Γ : Con i){j} → Ty Γ j → Con (i ⊔ j)
_▷_ Γ A = record
  { ∣_∣   = Σ ∣ Γ ∣ λ γ → ∣ EL A γ ∣
  ; _⊢_~_ = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ ⊢ γ ~ γ') λ γ~ → EL A γ' ⊢ ∣ subst A γ~ ∣ α ~ α' }
  ; ref   = λ { (γ ,Σ α) → ref Γ γ ,p subst-ref A α }
  ; sym   = λ { {γ ,Σ α}{γ' ,Σ α'}(γ~ ,p α~) → sym Γ γ~ ,p trans (EL A γ) (trans (EL A γ)
                                                               (~ (subst A (sym Γ γ~)) (sym (EL A γ') α~))
                                                               (sym (EL A γ) ((subst-trans A γ~ (sym Γ γ~)) α)))
                                                               (subst-ref A α) }
  ; trans = λ { {γ ,Σ α}{γ' ,Σ α'}{γ'' ,Σ α''}(γ~ ,p α~)(γ~' ,p α~') →
      trans Γ γ~ γ~' ,p trans (EL A γ'') (subst-trans A γ~ γ~' α) (trans (EL A γ'') (~ (subst A γ~') α~) α~') }
  }

_[_]T : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k} → Ty Δ k → Tms Γ Δ → Ty Γ k
A [ σ ]T = record {
  EL = λ γ → EL A (∣ σ ∣ γ) ;
  subst = λ γ~ → subst A (~ σ γ~) ;
  subst-ref = subst-ref A ;
  subst-trans = λ γ~ γ~' → subst-trans A (~ σ γ~) (~ σ γ~') }

id : ∀{i}{Γ : Con i} → Tms Γ Γ
id = record
  { ∣_∣ = λ γ → γ
  ; ~   = λ p → p
  }

_∘_ : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣ = λ γ → ∣ σ ∣ (∣ ν ∣ γ)
  ; ~   = λ p → ~ σ (~ ν p)
  }

ε : ∀{i}{Γ : Con i} → Tms Γ •
ε = record
  { ∣_∣ = λ _ → tt ;
    ~ = λ _ → ttp
  }

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Tms Γ Δ){b}{A : Ty Δ b} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = record { ∣_∣ = λ γ → ∣ σ ∣ γ ,Σ ∣ t ∣ γ ; ~ = λ p → ~ σ p ,p ~ t p }

π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ σ = record { ∣_∣ = λ γ → proj₁ (∣ σ ∣ γ) ; ~ = λ p → proj₁p (~ σ p) }

_[_]t : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣ = λ γ → ∣ t ∣ (∣ σ ∣ γ) ; ~ = λ p → ~ t (~ σ p) }

π₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ σ = record { ∣_∣ = λ γ → proj₂ (∣ σ ∣ γ) ; ~ = λ p → proj₂p (~ σ p) }
