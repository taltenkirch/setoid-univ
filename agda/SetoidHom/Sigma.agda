{-# OPTIONS --without-K --prop #-}

module SetoidHom.Sigma where

open import Agda.Primitive
open import SetoidHom.lib
open import SetoidHom.CwF

module _ {i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) where

  Σ' : Ty Γ (j ⊔ k)
  ∣ EL Σ' γ ∣ = Σ (∣ A ∣T γ) (λ α → ∣ B ∣T (γ ,Σ α))
  EL Σ' γ ⊢ (a ,Σ b) ~ (a' ,Σ b') = Σp (EL A γ ⊢ a ~ a') (λ p → EL B (γ ,Σ a') ⊢ ∣ subst B (ref Γ γ ,p trans (EL A γ) (~ (subst A _) p) (subst-ref A _)) ∣ b ~ b')
  ref (EL Σ' γ) (a ,Σ b) = ref (EL A γ) a ,p subst-ref B _
  sym (EL Σ' γ) {a ,Σ b} {a' ,Σ b'} (a~ ,p b~) = sym (EL A γ) a~ ,p
                          trans3 (EL B _) (sym (EL B (γ ,Σ a)) (~ (subst B (ref Γ _ ,p trans (EL A γ) (subst-ref A _) (sym (EL A _) a~))) b~))
                                           (sym (EL B (γ ,Σ a)) (subst-trans B _ _ _))
                                           (subst-ref B _)
  trans (EL Σ' γ) (a~ ,p b~) (a~' ,p b~') = (trans (EL A γ) a~ a~') ,p trans3 (EL B _) (subst-trans B _ _ _) (~ (subst B _) b~) b~'
  ∣ subst Σ' γ~ ∣ (a ,Σ b) = (∣ subst A γ~ ∣ a) ,Σ ∣ subst B (γ~ ,p (ref (EL A _) _)) ∣ b
  ~ (subst Σ' γ~) (a~ ,p b~) = ~ (subst A γ~) a~ ,p
                                trans3 (EL B _) (sym (EL B _) (subst-trans B _ _ _)) (subst-trans B _ _ _) (~ (subst B (γ~ ,p ref (EL A _) _)) b~)
  subst-ref Σ' (a ,Σ b) = subst-ref A a ,p trans (EL B _) (sym (EL B _) (subst-trans B _ _ _)) (subst-ref B _)
  subst-trans Σ' γ~ γ~' (a ,Σ b) = subst-trans A γ~ γ~' a ,p trans (EL B _) (sym (EL B _) (subst-trans B _ _ _)) (subst-trans B _ _ _)

  mkΣ' : (u : Tm Γ A)(v : Tm Γ (B [ _,_ id {A = A} u ]T)) → Tm Γ Σ'
  ∣ mkΣ' u v ∣ γ = ∣ u ∣ γ ,Σ ∣ v ∣ γ
  ~ (mkΣ' u v) γ~ = ~ u γ~ ,p trans (EL B _) (sym (EL B _) (subst-trans B _ _ _)) (~ v γ~)

  pr₁' : Tm Γ Σ' → Tm Γ A
  ∣ pr₁' t ∣ γ = proj₁ (∣ t ∣ γ)
  ~ (pr₁' t) γ~ = proj₁p (~ t γ~)

  pr₂' : (t : Tm Γ Σ') → Tm Γ (B [ _,_ id {A = A} (pr₁' t) ]T)
  ∣ pr₂' t ∣ γ = proj₂ (∣ t ∣ γ)
  ~ (pr₂' t) γ~ = trans (EL B _) (subst-trans B _ _ _) (proj₂p (~ t γ~))


_,Σ'_ = λ {i}{Γ}{j}{A}{k}{B} → mkΣ' {i}{Γ}{j} A {k} B
pr₁ = λ {i}{Γ}{j}{A}{k}{B} → pr₁' {i}{Γ}{j} A {k} B
pr₂ = λ {i}{Γ}{j}{A}{k}{B} → pr₂' {i}{Γ}{j} A {k} B
