{-# OPTIONS --without-K --prop #-}

module SetoidHom.SetsIR where

open import Agda.Primitive
open import SetoidHom.lib
open import SetoidHom.CwF

data U : Set₁
_~U_ : U → U → Prop₁
El : U → Set
El~ : (a : U) → El a → El a → Prop

data U where
  bool : U
  π    : (a : U)(b : El a → U)(refb : {x x' : El a}(x~ : El~ a x x') → b x ~U b x') → U
  σ    : (a : U)(b : El a → U)(refb : {x x' : El a}(x~ : El~ a x x') → b x ~U b x') → U

_~U_ = {!!}

El = {!!}

El~ = {!!}

U' : Con (lsuc lzero)
U' = record { ∣_∣ = U ; _⊢_~_ = _~U_ ; ref = {!!} ; sym = {!!} ; trans = {!!} }

El' : Ty U' lzero
El' = mkTy (λ A → record { ∣_∣ = El A ; _⊢_~_ = {!!} ; ref = {!!} ; sym = {!!} ; trans = {!!} }) {!!} {!!} {!!}

{-
data V : Set
EL : V → Set

U  : Ty Γ
El : Tm Γ U → Ty Γ
Π  : (a : Tm Γ U) → Ty (Γ▹El a) → Ty Γ
Π~ : (X : V) → (EL X → Ty Γ) → Ty Γ

P : parameter
Π~ : (P → Ty Γ) → Ty Γ

TODO Szumi: define this universe, show that it is closed under UP (check wether QITs work), ConTy (to check whether closed QIITs can be added)
TODO Ambrus: think about infinitary QIIT problem; can't we just use parameterised ToS (instead of Π~ ,Π^ with a universe)
-}
