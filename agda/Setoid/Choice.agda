{-# OPTIONS --without-K --prop #-}

module Setoid.Choice where

open import Agda.Primitive
open import Setoid.lib
open import Setoid.CwF
open import Setoid.Props
open import Setoid.Bool
open import Setoid.Pi

module _ {i}{Γ : Con i}{j}(A : Ty Γ j)(a : Tm Γ A) where

  Id : Ty (Γ ▷ A) (i ⊔ j)
  ∣ Id ∣T (γ* ,Σ a*) = ↑ps (↑pl {j}{i} (A T refC Γ γ* ⊢ ∣ a ∣t γ* ~ a*))
  Id T (γ₀₁ ,p α₀₁) ⊢ p₀ ~ p₁ = ↑pl ⊤p
  refT   Id = λ _ → mk↑pl (tr tt)
  symT   Id = λ _ → mk↑pl (tr tt)
  transT Id = λ _ _ → mk↑pl (tr tt)
  coeT   Id {γ₀ ,Σ a₀}{γ₁ ,Σ a₁}(γ₀₁ ,p a₀₁) (mk↑ps (mk↑pl e)) =
    mk↑ps (mk↑pl (transT A (transT A (symT A (~t a γ₀₁)) e) a₀₁))
  cohT   Id (_ ,p α₀₁) = λ _ → mk↑pl (tr tt)

  idp : Tm Γ (Id [ _,_ id {A = A} a ]T)
  ∣ idp ∣t γ = mk↑ps (mk↑pl (refT A (∣ a ∣t γ)))
  ~t idp γ~ = mk↑pl (tr tt)

p : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tms (Γ ▷ A) Γ
p {Γ = Γ} A = π₁ {_}{Γ ▷ A}{_}{Γ}{_}{A} id

v0 : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm (Γ ▷ A) (A [ p A ]T)
v0 A = π₂ {A = A} id

v1 : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → Tm (Γ ▷ A ▷ B) (A [ p A ∘ p B ]T)
v1 {A = A}{B = B} = π₂ {A = A} id [ p B ]t

-- this is one kind of truncation: the carrier is still the same
TR : ∀{i}{Γ : Con i}{j} → Ty Γ j → Ty Γ j
∣_∣T_ (TR A) = ∣ A ∣T_
_T_⊢_~_ (TR A) = λ _ _ _ → ↑pl ⊤p
refT (TR A) = λ _ → mk↑pl (tr tt)
symT (TR A) = λ _ → mk↑pl (tr tt)
transT (TR A) = λ _ _ → mk↑pl (tr tt)
coeT (TR A) = coeT A
cohT (TR A) = λ _ _ → mk↑pl (tr tt)

mkTR : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ A → Tm Γ (TR A)
∣ mkTR a ∣t γ = ∣ a ∣t γ
~t (mkTR a) _ = mk↑pl (tr tt)

unTR : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}(P : Ty (Γ ▷ TR A) k) →
  Tm (Γ ▷ TR A ▷ P ▷ P [ p P ]T) (Id (P [ p P ]T) (v0 P)) →
  Tm (Γ ▷ A) (P [ _,_ (p A) {A = TR A} (mkTR (v0 A)) ]T) →
  Tm (Γ ▷ TR A) P
∣ unTR P pP t ∣t = ∣ t ∣t
~t (unTR P pP t) {γ₀ ,Σ a₀}{γ₁ ,Σ a₁}(γ₀₁ ,p _) = transT P
  (cohT P (γ₀₁ ,p mk↑pl (tr tt)) (∣ t ∣t (γ₀ ,Σ a₀)))
  (un↑pl (un↑ps (∣ pP ∣t (γ₁ ,Σ a₁ ,Σ coeT P (γ₀₁ ,p mk↑pl (tr tt)) (∣ t ∣t (γ₀ ,Σ a₀)) ,Σ ∣ t ∣t (γ₁ ,Σ a₁)))))

-- the other kind of truncation: the carrier is now in Prop (this is the same as in Setoid.Props)
TR' : ∀{i}{Γ : Con i}{j} → Ty Γ j → Ty Γ j
∣_∣T_ (TR' A) γ = ↑ps (Tr (∣ A ∣T γ))
_T_⊢_~_ (TR' A) = λ _ _ _ → ↑pl ⊤p
refT (TR' A) = λ _ → mk↑pl (tr tt)
symT (TR' A) = λ _ → mk↑pl (tr tt)
transT (TR' A) = λ _ _ → mk↑pl (tr tt)
coeT (TR' A) {γ}{γ'} γ~ (mk↑ps a) = mk↑ps (untr {A = ∣ A ∣T γ}{λ _ → Tr (∣ A ∣T γ')} (λ a → tr (coeT A γ~ a)) a)
cohT (TR' A) = λ _ _ → mk↑pl (tr tt)

-- choice works with TR if the carrier is not quotiented (e.g. Bool, Nat)
boolChoice : ∀{i}{Γ : Con i}{j}(P : Ty (Γ ▷ Bool) j) →
  Tm (Γ ▷ Π Bool (TR P))
     (TR (Π Bool (P [ _,_ (π₁ {Δ = Γ}{A = Π Bool (TR P)} (π₁ {Δ = Γ ▷ Π Bool (TR P)}{A = Bool} id)) {A = Bool} (π₂ {Δ = Γ ▷ Π Bool (TR P)}{A = Bool} id) ]T)))
∣ boolChoice {_}{Γ} P ∣t (γ ,Σ (f ,sp _)) = f ,sp λ { tt tt _ → refT P (f tt) ; tt ff () ; ff tt () ; ff ff _ → refT P (f ff) }
~t (boolChoice {_}{Γ} P) _ = mk↑pl (tr tt)

-- choice doesn't work with TR'
boolChoice' : ∀{i}{Γ : Con i}{j}(P : Ty (Γ ▷ Bool) j) →
  Tm (Γ ▷ Π Bool (TR' P))
     (TR' (Π Bool (P [ _,_ (π₁ {Δ = Γ}{A = Π Bool (TR' P)} (π₁ {Δ = Γ ▷ Π Bool (TR' P)}{A = Bool} id)) {A = Bool} (π₂ {Δ = Γ ▷ Π Bool (TR' P)}{A = Bool} id) ]T)))
∣ boolChoice' {_}{Γ} P ∣t (γ ,Σ (f ,sp _)) = mk↑ps {!!}
~t (boolChoice' {_}{Γ} P) _ = mk↑pl (tr tt)

infixr 1 _⇒_ _⇛_ --   \r=   and \r==
_⇛_ = Π
_⇒_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) → Ty Γ (j ⊔ k)
A ⇒ B = A ⇛ (B [ p A ]T)

open import Setoid.Sigma

_×'_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) → Ty Γ (j ⊔ k)
A ×' B = Σ' A (B [ p A ]T)
infixl 5 _×'_

_⇔_ : ∀{i}{Γ : Con i}{j}(A B : Ty Γ j) → Ty Γ j
A ⇔ B = (A ⇒ B) ×' (B ⇒ A)

-- TODO: we should prove this with the eliminator of TR without unfolding
↔TR' : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
  (pA : Tm (Γ ▷ A ▷ A [ p A ]T) (Id (A [ p A ]T) (v0 A))) →
  Tm Γ (A ⇔ TR A)
∣ ↔TR' {_} {Γ} {_} {A} pA ∣t γ =
  ((λ a* → a*) ,sp (λ _ _ _ → mk↑pl (tr tt))) ,Σ
  ((λ a* → a*) ,sp λ a* a*' a~ → un↑pl (un↑ps (∣ pA ∣t (γ ,Σ a* ,Σ a*'))))
~t (↔TR' {_} {Γ} {_} {A} pA) {γ₀}{γ₁} γ₀₁ =
  (λ _ _ _ → mk↑pl (tr tt)) ,p
  (λ a₀ a₁ a₀₁ → transT A (cohT A γ₀₁ _) (un↑pl (un↑ps (∣ pA ∣t (γ₁ ,Σ coeT A γ₀₁ a₀ ,Σ a₁)))))

-- we have unique choice (this was noticed by e.g. Coquand in 2012)
-- TODO: we should prove this with the eliminator of TR without unfolding
uniqueChoice : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}(P : Ty (Γ ▷ A) k)
  (pP : Tm (Γ ▷ A ▷ P ▷ P [ p P ]T) (Id (P [ p P ]T) (v0 P))) →
  Tm (Γ ▷ A) (TR P) → Tm (Γ ▷ A) P
∣ uniqueChoice P pP t ∣t = ∣ t ∣t
~t (uniqueChoice P pP t) {γ₀ ,Σ a₀}{γ₁ ,Σ a₁}(γ₀₁ ,p a₀₁) =
  transT P (cohT P (γ₀₁ ,p a₀₁) (∣ t ∣t (γ₀ ,Σ a₀))) (un↑pl (un↑ps (∣ pP ∣t ((γ₁ ,Σ a₁) ,Σ coeT P (γ₀₁ ,p a₀₁) (∣ t ∣t (γ₀ ,Σ a₀)) ,Σ ∣ t ∣t (γ₁ ,Σ a₁)))))
